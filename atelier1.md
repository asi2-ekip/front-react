## Group EKIP
Membres du groupe :
- Kilian Decaderincourt
- Marceau Guyonnet
- Nicolas Ospina Sanchez
- Franck Velasco

Lien vers le group [Gitlab](https://gitlab.com/asi2-ekip).

- Franck :
  - Etude de l'application back-end monolithique existante
  - Initialisation projet Spring
  - Découpage de l'application en micro-services
    - Refonte du modèle de base de données, suppression des relations pour pouvoir supporter une architecture micro-services
    - Mise en place d'un gateway pour exposer les APIs REST et pour la communication directe entre les services
  - Mise en place et utilisation du bus de communication ActiveMQ
  - Création Dockerfile et Docker compose (avec l'aide de Kilian) avec :
    - Un conteneur par micro-service Spring
    - Un conteneur pour la base de données postgresql & un conteneur pgadmin pour pouvoir administrer la base
    - Un conteneur pour le bus de communication ActiveMQ
    - Un conteneur pour le gateway nginx
- Kilian :
  - Initialisation projet React
  - Système de Navigation React
  - Gestion utilisateur connecté React
  - Dockerfile et Docker compose
  - Externalisation de la configuration en variables d'environements pour le Front
  - Chaine CI
- Marceau :
  - Tableau comparatif FrameWork FrontEnd
  - Inscription de l'utilisateur React
  - Connexion de l'utilisateur React
  - Architecture React
- Nicolas :
  - Développement des composants react Card/Buy/Sell/Shop
  - Développement des actions et Reducers liés au composants
  - Récupération des données backend pour la liaison du shop et du backend (bdd)



Liste des éléments réalisés : 
- Transformer le BackEnd #TODO préciser
- Connexion de l'utilisateur
- Inscription d'un utilisateur
- Attribution de carte à l'utilisateur
- Possiblité d'acheter des cartes parmi une liste
- Possibilité de vender les carets parmi une liste

Liste des éléments non-réalisés:
- Écran d'accueil