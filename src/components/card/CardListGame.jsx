import React from 'react';
import Grid from "@material-ui/core/Grid";
import {Card} from "./Card";
export function CardListGame(props){
    return (
        <span>
            {props.cards.map((card) => (
                    <Grid item xs={2}>
                <Card card={card}/>
                    </Grid>
            ))}
        </span>
    );
}
