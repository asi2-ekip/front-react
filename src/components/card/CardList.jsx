import { makeStyles, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import React, { Component } from 'react';
import { CardRow } from './CardRow';

const useStyles = makeStyles({
    table: {
        width: "auto",
    },
  });

//<table className="ui selectable celled table" id="cardListId">

export function CardList(props){

    const classes = useStyles();

    return (
        <TableContainer component={Paper} >
            <Table className={classes.table} aria-label="simple">
                <TableHead>
                    <TableRow>
                        <TableCell>Image</TableCell>
                        <TableCell>Name</TableCell>
                        <TableCell>Description</TableCell>
                        <TableCell>Family</TableCell>
                        <TableCell>HP</TableCell>
                        <TableCell>Energy</TableCell>
                        <TableCell>Defence</TableCell>
                        <TableCell>Attack</TableCell>
                        <TableCell>Price</TableCell>
                        <TableCell></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.cards.map((card) => (
                        <CardRow card={card}/>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
    
}
