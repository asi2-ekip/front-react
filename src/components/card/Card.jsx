import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CardUi from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FlashOnIcon from '@material-ui/icons/FlashOn';
import ColorizeIcon from '@material-ui/icons/Colorize';
import LockIcon from '@material-ui/icons/Lock';
import CardMedia from '@material-ui/core/CardMedia';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import LocalAtmIcon from '@material-ui/icons/LocalAtm';
import {CardConfirm} from "./CardConfirm";
import SecurityIcon from "@material-ui/icons/Security";
import {Icon} from "@iconify/react";
import swordIcon from "@iconify/icons-mdi/sword";



const useStyles = makeStyles((theme) => ({
    root: {
        width: "auto",
        marginRight:20,
    },
    textField: {
        width: "auto",
        marginTop: 15,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    rootGrid: {
        flexGrow: 1,
        marginTop: 15,
        marginBottom: 15,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    divider: {
        marginTop: 15,
        marginBottom: 15,
    },
    button: {
        width: "auto",
        alignItems: "center"
    },
    snackbar: {
    width: '100%',
    '& > * + *': {
        marginTop: theme.spacing(2),
    },
},
}));

export function Card(props) {
    const classes = useStyles();
    return (
        <CardUi className={classes.root}>
            <CardContent>
                <div className={classes.rootGrid}>
                    <Grid container>
                        <Grid item xs>
                            <FavoriteIcon/>{props.card.hp}
                        </Grid>
                        <Grid item xs>
                            {props.card.family}
                        </Grid>
                        <Grid item xs>
                            <FlashOnIcon/>{props.card.energy}
                        </Grid>
                    </Grid>
                </div>
                <CardMedia
                    className={classes.media}
                    image={props.card.imgUrl}
                    title="Paella dish"
                />
                <div className={classes.textField}>
                    <TextField
                        id="outlined-multiline-static"
                        label="Description"
                        fullWidth={true}
                        multiline
                        defaultValue={props.card.description}
                        variant="outlined"
                        InputProps={{
                            readOnly: true,
                        }}
                    />
                </div>
                <div className={classes.rootGrid}>
                    <Grid container>
                        <Grid item xs>
                            <FavoriteIcon/>{props.card.hp}
                        </Grid>
                        <Grid item xs={6}>
                        </Grid>
                        <Grid item xs>
                            <FlashOnIcon/>{props.card.energy}
                        </Grid>
                    </Grid>
                </div>
                <Divider classes={classes.divider}/>
                <div className={classes.rootGrid}>
                    <Grid container>
                        <Grid item xs>
                            <SecurityIcon/>{props.card.defence}
                        </Grid>
                        <Grid item xs={6}>
                        </Grid>
                        <Grid item xs>
                            <Icon icon={swordIcon} style={{ fontSize: 25 }}/>{props.card.attack}
                        </Grid>
                    </Grid>
                </div>
                <Divider classes={classes.divider}/>
            </CardContent>
            <CardActions>
                <div><LocalAtmIcon/>Actual value {props.card.price}$</div>
            </CardActions>
            <CardConfirm card={props.card} from={props.from}></CardConfirm>
        </CardUi>

    )
}