import { Avatar, IconButton, TableCell, TableRow } from '@material-ui/core';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { useDispatch, useSelector } from 'react-redux'
import { setSelectedCard } from '../../actions/storeAction'
import React, {Component} from 'react';


export function CardRow(props){
    const dispatch = useDispatch()
    const onClick = () => {
        dispatch(setSelectedCard(props.card))
    }

    return (
        <TableRow onClick={onClick} hover={true}>
            <TableCell>
                <Avatar src={props.card.smallImgUrl}></Avatar>
            </TableCell>
            <TableCell>{props.card.name}</TableCell>
            <TableCell>{props.card.description}</TableCell>
            <TableCell>{props.card.family}</TableCell>
            <TableCell>{props.card.hp}</TableCell>
            <TableCell>{props.card.energy}</TableCell>
            <TableCell>{props.card.defense}</TableCell>
            <TableCell>{props.card.attack}</TableCell>
            <TableCell>{props.card.price}</TableCell>
            {/*<TableCell>*/}
            {/*    <IconButton aria-label="sell">*/}
            {/*        <ShoppingCartIcon/>*/}
            {/*    </IconButton>*/}
            {/*</TableCell>*/}
        </TableRow>

    );
}

