import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import {useDispatch, useSelector} from "react-redux";
import {setCardsToBuy, setSelectedCard, setUserCards} from "../../actions/storeAction";
import {setSelectedCards} from "../../actions/playAction";
import {loginAction} from "../../actions/loginAction";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
    root: {
        width: "auto",
        marginRight:20,
    },
    button: {
        width: "auto",
        alignItems: "center"
    },
    rootGrid: {
        flexGrow: 1,
        marginTop: 15,
        marginBottom: 15,
    },
}));

export function CardConfirm(props) {
    const [openSuccess, setOpenSuccess] = React.useState(false);
    const [openError, setOpenError] = React.useState(false);

    const classes = useStyles();
    const currentUser = useSelector((state) => state.loggedUserReducer.user)

    const cardsToBuy = useSelector((state) => state.storeReducer.cardsToBuy)
    const userCards = useSelector((state) => state.storeReducer.userCards)
    const playSelectedCards = useSelector((state) => state.playReducer.selectedCards)
    const dispatch = useDispatch()
    const onClick = async (e) => {
        if (props.from === "Buy" || props.from === "Sell") {
            var url = ''
            if (props.from === "Buy") {
                url = 'http://localhost/api/store/buy'
            } else if (props.from === "Sell") {
                url = 'http://localhost/api/store/sell'
            }
            const responseBuy = await fetch(url, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                },
                body: JSON.stringify({
                    userId: currentUser.id,
                    cardId: props.card.modelId,
                })
            })
                .then(response => response.json())
                .then((result) => {
                    if (result === true) {
                        fetch("http://localhost/api/user/" + currentUser.id)
                            .then(res =>
                                res.json()
                            ).then((result) => {
                            const infos = {
                                user: {
                                    id: result.id,
                                    login: result.login,
                                    account: result.account,
                                    lastName: result.lastName,
                                    surName: result.surName,
                                    email: result.email
                                },
                                token: 'abcde'
                            }
                            dispatch(loginAction(infos))
                        })
                        if (props.from === "Buy") {
                            cardsToBuy.splice(cardsToBuy.indexOf(props.card), 1)
                            dispatch(setCardsToBuy([...cardsToBuy]))
                            dispatch(setSelectedCard(undefined))
                        } else if (props.from === "Sell") {
                            userCards.splice(userCards.indexOf(props.card), 1)
                            dispatch(setUserCards([...userCards]))
                            dispatch(setSelectedCard(undefined))
                        }
                        setOpenSuccess(true);
                    } else {
                        setOpenError(true);
                    }
                })
        } else if (props.from === "Play") {
            if (e.currentTarget.id === "select"){
                if (playSelectedCards.indexOf(props.card) === -1 && playSelectedCards.length < 5){
                    dispatch(setSelectedCards([...playSelectedCards,props.card]))
                    setOpenSuccess(true);
                }else{
                    setOpenError(true);
                }
            }else if(e.currentTarget.id === "unselect"){
                if (playSelectedCards.indexOf(props.card) !== -1){
                    playSelectedCards.splice(playSelectedCards.indexOf(props.card),1)
                    dispatch(setSelectedCards([...playSelectedCards]))
                }else{
                }
            }
        }
    }

    const onClickNOA = (e) => {
        if (e.currentTarget.id === "select"){
            if (playSelectedCards.indexOf(props.card) === -1 && playSelectedCards.length < 5){
                playSelectedCards.push(props.card)
                dispatch(setSelectedCards(playSelectedCards))
                setOpenSuccess(true);
            }else{
                setOpenError(true);
            }
        }else if(e.currentTarget.id === "unselect"){
            if (playSelectedCards.indexOf(props.card) !== -1){
                playSelectedCards.splice(playSelectedCards.indexOf(props.card),1)
                dispatch(setSelectedCards(playSelectedCards))
            }else{
            }
        }
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenSuccess(false);
        setOpenError(false);
    };
    function MessageSuccess(props){
        if(props.from === "Buy"){
            return (
                <div>Vous avez bien acheté la carte !</div>
            )
        }else if(props.from === "Sell"){
            return (
                <div>Votre carte a bien été vendue !</div>
            )
        }
        else if(props.from === "Play"){
            return (
                <div>Votre carte a bien été choisie !</div>
            )
        }
    }
    function MessageError(props){
        if(props.from === "Buy"){
            return (
                <div>La carte n'a pas pu être achetée.</div>
            )
        }else if(props.from === "Sell"){
            return (
                <div>Votre carte n'as pas pu être vendue.</div>
            )
        }
        else if(props.from === "Play"){
            return (
                <div>Cette carte a déjà été choisie ou vous avez déjà 5 cartes</div>
            )
        }
    }
    function CardButton(props){
        if(props.from === "Buy"){
            return (
                <Button variant="contained" fullWidth={true} size="large" className={classes.button} onClick={onClick}>BUY CARD</Button>
            )
        }else if(props.from === "Sell"){
            return (
                <Button variant="contained" fullWidth={true} size="large" className={classes.button} onClick={onClick}>SELL CARD</Button>
            )
        }
        else if(props.from === "Play"){
            return (
                <div className={classes.rootGrid}>
                    <Grid container>
                        <Grid item xs>
                            <Button id="select" variant="contained" fullWidth={true} size="large" className={classes.button} onClick={onClick}>SELECT CARD</Button>
                        </Grid>
                        <Grid item xs={6}>
                        </Grid>
                        <Grid item xs>
                            <Button id="unselect" variant="contained" fullWidth={true} size="large" className={classes.button} onClick={onClick}>UNSELECT CARD</Button>
                        </Grid>
                    </Grid>
                </div>
            )
        }
    }
    if (props.from !== undefined) {
        return (
            <div>
                <CardActions>
                    <CardButton from={props.from}></CardButton>
                </CardActions>
                <div className={classes.root}>
                    <Snackbar open={openSuccess} autoHideDuration={3000} onClose={handleClose}>
                        <Alert onClose={handleClose} severity="success">
                            <MessageSuccess from={props.from}></MessageSuccess>
                        </Alert>
                    </Snackbar>
                    <Snackbar open={openError} autoHideDuration={3000} onClose={handleClose}>
                        <Alert onClose={handleClose} severity="error">
                            <MessageError from={props.from}></MessageError>
                        </Alert>
                    </Snackbar>
                </div>
            </div>
        )
    }else{
        return (<div></div>)
    }
}