import React, {Component} from 'react';
import CardUi from "@material-ui/core/Card";
import {makeStyles} from "@material-ui/core/styles";
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FlashOnIcon from '@material-ui/icons/FlashOn';
import SecurityIcon from '@material-ui/icons/Security';
// npm install --save-dev @iconify/react @iconify/icons-mdi
import { Icon, InlineIcon } from '@iconify/react';
import swordIcon from '@iconify/icons-mdi/sword';

import CardMedia from '@material-ui/core/CardMedia';
const useStyles = makeStyles((theme) => ({
    root: {
        width: "auto",
        marginRight:20,
    },
    textField: {
        width: "auto",
        marginTop: 15,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    rootGrid: {
        flexGrow: 1,
        marginTop: 15,
        marginBottom: 15,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    divider: {
        marginTop: 15,
        marginBottom: 15,
    },
    buttonBuy: {
        width: "auto",
        alignItems: "center"
    }

}));


export function Cardshort(props) {
    const classes = useStyles();
    return (
        <CardUi className={classes.root}>
            <CardContent>

                <CardMedia
                    className={classes.media}
                    image={props.card.imgUrl}
                    title="Paella dish"
                />
                <div className={classes.rootGrid}>
                    <Grid item xs>
                        {props.card.family}
                    </Grid>
                    <Grid container>
                        <Grid item xs>
                            <FavoriteIcon/>{props.card.hp}
                        </Grid>
                        <Grid item xs>
                            <FlashOnIcon/>{props.card.energy}
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid item xs>
                            <SecurityIcon/>{props.card.defence}
                        </Grid>
                        <Grid item xs>
                            <Icon icon={swordIcon} style={{ fontSize: 25 }}/>{props.card.attack}
                        </Grid>
                    </Grid>
                </div>
            </CardContent>
        </CardUi>
    )
}
