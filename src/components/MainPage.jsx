import { useSelector } from "react-redux"
import { navigationPage } from "../reducers/navigationReducer"
import { Login } from "./login/Login"
import { Register } from "./register/Register"
import { Sell } from "./sell/Sell"
import { Buy } from "./buy/Buy"
import { Chat } from "./chat/Chat"
import {Play} from "./play/Play";
import {Game} from "./play/Game";

export function MainPage(){

    const currentPage = useSelector((state) => state.navigationReducer.page)


    const pageComponents = new Map([
        [navigationPage.LOGIN, Login],
        [navigationPage.REGISTER, Register],
        [navigationPage.SELLCARDS, Sell],
        [navigationPage.BUYCARDS, Buy],
        [navigationPage.CHAT, Chat],
        [navigationPage.PLAY, Play],
        [navigationPage.GAME, Game]
    ])

    const Component = pageComponents.get(currentPage)

    return <Component/>


}