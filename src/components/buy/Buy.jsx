import React  from 'react'
import { useSelector} from "react-redux";
import {Store} from "../store/Store";

export function Buy() {
    return (
        <Store from={"Buy"} url={"/api/cards_to_sell"}/>
    );
}
