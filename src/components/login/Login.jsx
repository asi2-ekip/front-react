import React, { useState } from "react";
import { Button, Avatar, CssBaseline, makeStyles, Container, Typography, TextField } from '@material-ui/core';
import { LockOutlined } from "@material-ui/icons";
import { useDispatch } from "react-redux";
import { loginAction } from "../../actions/loginAction";
import { navigateAction } from "../../actions/navigateAction";
import { navigationAction, navigationPage } from "../../reducers/navigationReducer";
import { ConfigEnum, ConfigService } from "../../services/Config.service";


const useStyle = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
    },
    root: {
        widht: "100%",
        marginTop: theme.spacing(2),
    },
}));




export function Login() {

    const [login, setLogin] = useState("")
    const [password, setPassword] = useState("")

    const classes = useStyle();

    const dispatch = useDispatch()


    const handleLoginChange = (event) => {
        setLogin(event.target.value)

    }


    const handlePasswordChange = (event) => {
        let password = event.target.value;
        setPassword(password);
    }


    const onLoginClick = async () => {
        if (!password) {
            return;
        }
        const apiUrl = await ConfigService.get(ConfigEnum.API_URL)

        const loginResult = await fetch(`${apiUrl}/api/auth/login`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify({
                login: login,
                pwd: password,
            })
        }).then(res => res.json())
        .catch(err => console.log(err))

            
        if (!loginResult.userId > 0) {
            return;
        }

        const userResult = await fetch(`${apiUrl}/api/user/` + loginResult.userId)
            .then(res => res.json())

        const infos = {
            user: {
                id: userResult.id,
                login: userResult.login,
                account: userResult.account,
                lastName: userResult.lastName,
                surName: userResult.surName,
                email: userResult.email
            },
            token: 'abcde'
        }
        dispatch(loginAction(infos))
        dispatch(navigateAction(navigationPage.SELLCARDS))

    }

    const onCancelClick = () => {
        setLogin("");
        setPassword("");
    }

    return (
        <Container component="main" maxWidth="s">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlined />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Login
                </Typography>
                <form className={classes.root} >
                    <TextField required fullWidth margin="normal" id="standard-login" value={login} onChange={handleLoginChange} label="Login" />
                    <TextField required fullWidth margin="normal" type="password" id="standard-password" value={password} onChange={handlePasswordChange} label="Password" />
                    <Button fullWidth variant="contained" color="primary" className="btnLogin" onClick={onLoginClick}>
                        Login
                    </Button>
                    <Button fullWidth variant="contained" className="btnCancel" onClick={onCancelClick}>
                        Cancel
                    </Button>

                </form>
            </div>
        </Container>
    )

}