import React, {useState} from "react";
import {Button, Avatar, CssBaseline, makeStyles, Container, Typography, TextField} from '@material-ui/core';
import {LockOutlined} from "@material-ui/icons";
import { navigateAction } from "../../actions/navigateAction";
import { navigationPage } from "../../reducers/navigationReducer";
import { useDispatch } from "react-redux";
import { ConfigEnum, ConfigService } from "../../services/Config.service";

const useStyle = makeStyles((theme) => ({
    paper:{
        marginTop:theme.spacing(8),
        display:'flex',
        flexDirection:'column',
        alignItems:'center'

    },
    avatar:{
        margin:theme.spacing(1),
    },
    root:{
        widht:"100%",
        marginTop:theme.spacing(1),
    },
  }));
  

export function Register(){
    
    const [password_strength, setPassword_strength] = useState("")
    const [surname, setSurname] = useState("")
    const [username, setUsername] = useState("")
    const [lastname, setLastname] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [repassword, setRepassword] = useState("")

    const classes = useStyle();
    const dispatch = useDispatch()

    const handleUsernameChange = (event) =>{
        setUsername(event.target.value)
    }

    const handleSurnameChange = (event) =>{
        setSurname(event.target.value)
    }

    const handleLastnameChange = (event) =>{
        setLastname(event.target.value)
    }

    const handleEmailChange = (event) =>{
        setEmail(event.target.value)
    }

    const handlePasswordChange = (event) =>{

        let password = event.target.value;
        setPassword(password);
        let secuMsg = ""
        if (password.length < 4){
            secuMsg = "weak"
        } else if(password.length >= 4 && password.length < 8) {
            secuMsg="medium"
        } else if(password.length >= 8) {
            secuMsg="strong"
        }
        setPassword_strength(secuMsg);
    }

    const handleRePasswordChange = (event) => {
        setRepassword(event.target.value);
    }

    const onRegisterClick = async () => {
        if(!password || password !== repassword){
            return;
        }

        const apiUrl = await ConfigService.get(ConfigEnum.API_URL)

        fetch(`${apiUrl}/api/auth/register`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                login: surname,
                pwd: password,
                email: email,
                surName: surname,
                lastName: lastname
            })
        }).then(res => { //TODO Check REGISTER success 200 before redirecting to LOGIN page
            dispatch(navigateAction(navigationPage.LOGIN))
        });
    }

    const onCancelClick = () => {
        setUsername("");
        setLastname("");
        setSurname("");
        setEmail("");
        setPassword("");
        setRepassword("");
        setPassword_strength("");
      }

    return (
        <Container component="main" maxWidth="s">
            <CssBaseline/>
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlined/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    Register
                </Typography>
                <form className={classes.root} >
                    <TextField margin="normal" fullWidth required id="standard-username" label="Username" value={username} onChange={handleUsernameChange}/>

                    <TextField margin="normal" fullWidth required id="standard-lastname" label="Lastname" value={lastname} onChange={handleLastnameChange}/>
                    <TextField margin="normal" fullWidth required id="standard-surname" label="Surname" value={surname} onChange={handleSurnameChange}/>

                    <TextField margin="normal" fullWidth required type="email" id="standard-email" label="Email" value={email} onChange={handleEmailChange}/>

                    <TextField margin="normal" fullWidth required type="password" id="standard-password" value={password} label="Password" onChange={handlePasswordChange} />
                    <p>{password_strength}</p>
                    <TextField margin="normal" fullWidth required type="password" id="standard-re-password" value={repassword} label="Re-Password" onChange={handleRePasswordChange}/>

                    <Button margin="normal" fullWidth variant="contained" color="primary" className="btnRegister"  onClick={onRegisterClick}>
                        Register 
                    </Button>
                    <Button margin="normal" fullWidth variant="contained" className="btnCancel" onClick={onCancelClick}>
                        Cancel 
                    </Button>
                    
                </form>
            </div>
        </Container>
    )

}
