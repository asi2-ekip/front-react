import React, { useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux'
import Grid from '@material-ui/core/Grid';
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import Paper from "@material-ui/core/Paper";
import {Card} from "../card/Card";
import {PlayerInfos} from "./PlayerInfos";
import {Cardshort} from "../card/Cardshort";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginTop: 20,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    divider: {
        marginTop: 15,
        marginBottom: 15,
    },
    text:{
        textAlign:"center"
    },
    button:{
        marginLeft: "30%"
    }
}));

export function Game() {
    const classes = useStyles();
    const selectedCards = useSelector((state) => state.playReducer.selectedCards)
    const dispatch = useDispatch()
    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Grid container spacing={0}>
                        <Grid item xs={2}>
                            <PlayerInfos pseudo={"test pseudo"}></PlayerInfos>
                        </Grid>
                        {selectedCards.map((card) => (
                            <Grid item xs={2}>
                                <Cardshort card={card}/>
                            </Grid>
                        ))}
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Grid container spacing={0}>
                        <Grid item xs={2}>
                            <Button variant="contained"  size="large" className={classes.button}>Attack</Button>
                        </Grid>
                        <Grid item xs={10}>
                            <Divider/>
                            <Paper className={classes.text}>VS</Paper>
                            <Divider/>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Grid container spacing={0}>
                        <Grid item xs={2}>
                            <PlayerInfos pseudo={"test pseudo"}></PlayerInfos>
                        </Grid>
                        {selectedCards.map((card) => (
                            <Grid item xs={2}>
                                <Cardshort card={card}/>
                            </Grid>
                        ))}
                    </Grid>
                </Grid>
            </Grid>

        </div>
    )
}
