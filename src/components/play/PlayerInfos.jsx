import React from 'react';
import CardContent from "@material-ui/core/CardContent";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import Typography from "@material-ui/core/Typography";
import CardUi from "@material-ui/core/Card";
import {makeStyles} from "@material-ui/core/styles";
import withStyles from "@material-ui/core/styles/withStyles";
import LinearProgress from "@material-ui/core/LinearProgress";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginTop: 20,
    },
}));
const BorderLinearProgress = withStyles((theme) => ({
    root: {
        height: 30,
        borderRadius: 5,
    },
    colorPrimary: {
        backgroundColor: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
    },
    bar: {
        borderRadius: 5,
        backgroundColor: '#1a90ff',
    },
}))(LinearProgress);
export function PlayerInfos(props){
    const classes = useStyles();
    return (
        <CardUi className={classes.root}>
            <CardContent>
                <AccountCircleIcon style={{ fontSize: 60 }}/>
                <Typography variant="h6" component="h2">
                    {props.pseudo}
                </Typography>
                <BorderLinearProgress variant="determinate" value={50} />
                <Typography variant="h6" component="h2" style={{ textAlign: "center" }}>
                    Actions Points
                </Typography>
            </CardContent>
        </CardUi>
    );
}
