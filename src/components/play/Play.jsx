import React, { useEffect } from 'react'
import {CardList} from '../card/CardList'
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux'
import Grid from '@material-ui/core/Grid';
import {setSelectedCard, setUserCards} from '../../actions/storeAction'
import {Card} from "../card/Card";
import { ConfigEnum, ConfigService } from '../../services/Config.service';
import {setSelectedCards} from "../../actions/playAction";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import {navigateAction} from "../../actions/navigateAction";
import {navigationPage} from "../../reducers/navigationReducer";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginTop: 20
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

export function Play(props) {
    const classes = useStyles();
    const userCards = useSelector((state) => state.storeReducer.userCards)
    const currentUser = useSelector((state) => state.loggedUserReducer.user)
    const selectedCards = useSelector((state) => state.playReducer.selectedCards)
    const dispatch = useDispatch()
    useEffect(() => {
        const fetchData = async () => {
            const apiUrl = await ConfigService.get(ConfigEnum.API_URL)
            const resp_cards = await fetch(
                `${apiUrl}/${"/api/card/user/"+currentUser.id}`
            )
            const cardsToDisplay = await resp_cards.json()
            dispatch(setUserCards(cardsToDisplay))
            dispatch(setSelectedCard(undefined))
            dispatch(setSelectedCards(selectedCards))
        }
        fetchData()
    }, [dispatch])

    if (userCards.length < 1) {
        return <div>pas de carte</div>
    }

    function SelectedCard(props) {
        const selectedCard = useSelector((state) => state.storeReducer.selectedCard)
        if (selectedCard === undefined) {
            return <div/>
        }
        return (
            <Card card={selectedCard} from={"Play"}/>
        )
    }
    const onClick = () => {
        if (selectedCards.length > 0){
            console.log("j'ai " + selectedCards.length + " cartes")
            dispatch(navigateAction(navigationPage.GAME))
        }else {
            console.log("je n'ai pas assez de cartes")
        }
    }
        return (
            <div>
                <Button variant="contained" fullWidth={true} size="large" className={classes.button} onClick={onClick}>Confirm</Button>
                <div className={classes.root}>
                    <Grid container spacing={3}>
                        <Grid item xs={24} sm={9}>
                            <Typography variant="h4" component="h2">
                                My cards
                            </Typography>
                            <CardList cards={userCards}/>
                            <Typography variant="h4" component="h2">
                                Selected cards
                            </Typography>
                            <CardList cards={selectedCards}/>
                        </Grid>
                        <Grid item xs={6} sm={3}>
                            <SelectedCard from={"Play"}/>
                        </Grid>
                    </Grid>
                </div>

            </div>
        )
}
