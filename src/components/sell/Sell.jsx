import React  from 'react'
import { useSelector } from 'react-redux'
import {Store} from "../store/Store";

export function Sell() {
    const currentUser = useSelector((state) => state.loggedUserReducer.user)
    return (
        <Store from={"Sell"} url={"/api/card/user/"+currentUser.id}/>
    );
}
