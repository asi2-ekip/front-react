import { Grid, makeStyles} from '@material-ui/core'
import React, { Fragment, useEffect, useState } from 'react'
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { ChatMessage } from './ChatMessage';
import { ChatService } from '../../services/Chat.service';
import { useDispatch, useSelector } from 'react-redux';
import { SelectUser } from './SelectUser';
import { Input } from './Input';

const msgMock = [
    { name: 'Jean Dupont', msg: 'blalblabla' },
    { name: 'Luc Dupont', msg: 'blalblabla' },
    { name: 'Stephane Dupont', msg: 'blalblabla' }
]

const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
        justify:'space-between'
    }
}));

export function Conversation() {

    const classes = useStyles();
    const dispatch = useDispatch();

    const loggedUser = useSelector((state) => state.loggedUserReducer.user)
    const chats = useSelector((state) => state.chatReducer.chats)
    const selectedChat = useSelector((state) => state.chatReducer.selectedChat)

    useEffect(() => {
        console.log(selectedChat);
        console.log(chats);
        setChat(chats[selectedChat] || [])
    }, [chats, selectedChat])



    const [chat, setChat] = useState(msgMock);
    
    return (
        <Fragment>
            {chat.map((msg) => (
                <ChatMessage message={msg} currentUser={loggedUser}/>
            ))}
        </Fragment>

    )
}
