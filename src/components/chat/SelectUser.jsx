import { MenuItem, Select } from '@material-ui/core'
import React, { useEffect, useState } from 'react'

import { useDispatch, useSelector } from 'react-redux';
import { selectAction } from '../../actions/selectAction';
import { ChatService } from '../../services/Chat.service';


export function SelectUser() {

    const dispatch = useDispatch();

    console.log('redraw Select user');
    


    const selectedChat = useSelector((state) => state.chatReducer.selectedChat)

    const onSelectChange = (selection) => {
        dispatch(selectAction(selection.target.value))
    }

    const [users, setUsers] = useState([]);

    useEffect(() => {
        ChatService.getUsers()
            .then(users => setUsers(users))
    }, [])



    return (
            <Select value={selectedChat} onChange={onSelectChange}>
                <MenuItem value={"global"}>Global</MenuItem>
                {users.map((user) => (
                    <MenuItem value={user}>{user}</MenuItem>
                ))}
            </Select>
    )
}
