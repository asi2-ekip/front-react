import { Button, Grid, makeStyles, TextField } from '@material-ui/core'
import React, { useState } from 'react'
import SendIcon from '@material-ui/icons/Send';
import { ChatService } from '../../services/Chat.service';
import { useDispatch, useSelector } from 'react-redux';
import { newMessageAction } from '../../actions/newMessageAction';


const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
        justify:'space-between'
    },
    textarea: {
        width: '100%',
    },
    sendbtn: {
        width: '100%',
        text: 'center',
    }
}));

export function Input(props) {

    const classes = useStyles();
    const dispatch = useDispatch();
    
    const selectedChat = useSelector((state) => state.chatReducer.selectedChat)

    const [chatForm, setChatForm] = useState('');

    const handleChatFormChange = (event) => {
        setChatForm(event.target.value)
    }

    const sendChatMessage = () => {
        if(selectedChat === 'global') {
            ChatService.sendGlobal(chatForm)
        } else {
            ChatService.sendPrivate(selectedChat, chatForm)
            dispatch(newMessageAction({
                dest: selectedChat,
                source: props.username,
                msg: chatForm
            }))
        }

        setChatForm('')
    }

    return (
            <Grid container>
                <Grid item xs={8} sm={10}>
                        <TextField className={classes.textarea} variant="outlined" value={chatForm} onChange={handleChatFormChange} />
                </Grid>
                <Grid item xs={4} sm={2}>
                    <Button color={"primary"} className={classes.sendbtn} onClick={sendChatMessage}>
                        Send<SendIcon />
                    </Button>
                </Grid>
            </Grid>

    )
}
