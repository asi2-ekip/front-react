import { Card, CardContent, Typography, makeStyles } from '@material-ui/core'
import React, { useState } from 'react'

const useStyles = makeStyles(() => ({
    personalMsg: {
        color:"textPrimary",
    },
    externalMsg: {
        color:"textSecondary",
        gutterBottom:'true',
    }
}));



export function ChatMessage(props) {
    const classes = useStyles();


    return (
            <Card variant='outlined' align={props.currentUser.login !== props.message.source ? 'left' : 'right'}>
                <CardContent>
                <React.Fragment>
                    <Typography className={props.currentUser.login !== props.message.source ? classes.externalMsg : classes.personalMsg}>
                        {props.message.source}
                    </Typography>
                    <Typography className={props.currentUser.login !== props.message.source ? classes.externalMsg : classes.personalMsg} variant="h5" component="h2">
                        {props.message.msg}
                    </Typography>
                </React.Fragment>
                </CardContent>
            </Card>
        )
}