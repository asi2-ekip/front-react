import { Grid, makeStyles } from '@material-ui/core'
import React, { Fragment, useEffect, useState } from 'react'
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { ChatService } from '../../services/Chat.service';
import { useDispatch, useSelector } from 'react-redux';
import { SelectUser } from './SelectUser';
import { Input } from './Input';
import { Conversation } from './Conversation';
import { newMessageAction } from '../../actions/newMessageAction';

const msgMock = [
    { name: 'Jean Dupont', msg: 'blalblabla' },
    { name: 'Luc Dupont', msg: 'blalblabla' },
    { name: 'Stephane Dupont', msg: 'blalblabla' }
]

const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
        justify: 'space-between'
    },
    textarea: {
        width: '100%',
    },
    sendbtn: {
        width: '100%',
        text: 'center',
    }
}));

export function Chat() {

    const classes = useStyles();
    const dispatch = useDispatch();

    const loggedUser = useSelector((state) => state.loggedUserReducer.user)

    useEffect(() => {
        ChatService.init()
            .then(() => ChatService.auth(loggedUser.login))
            .then(() => {

                ChatService.socket.on('globalMessage', msgReceived => {
                    dispatch(newMessageAction({
                        dest: 'global',
                        source: msgReceived.username,
                        msg: msgReceived.msg
                    }))
                })

                ChatService.socket.on('privateMessage', msgReceived => {
                    console.log(msgReceived);
                    dispatch(newMessageAction({
                        dest: msgReceived.username,
                        source: msgReceived.username,
                        msg: msgReceived.msg
                    }))
                })
            })



        return () => {
            ChatService.socket.removeListener('globalMessage')
            ChatService.socket.removeListener('privateMessage')
        }
    }, [])


    return (
        <Fragment>
            <Grid container className={classes.root}>
                <Grid item>Chat</Grid>
                <Grid item>{loggedUser.login} <AccountCircleIcon /></Grid>
            </Grid>
            <SelectUser />

            <Conversation />

            <Input username={loggedUser.login}/>

        </Fragment>

    )
}
