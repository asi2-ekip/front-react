import React, { useEffect } from 'react'
import {CardList} from '../card/CardList'
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux'
import Grid from '@material-ui/core/Grid';
import {setCardsToBuy, setSelectedCard, setUserCards} from '../../actions/storeAction'
import {Card} from "../card/Card";
import { ConfigEnum, ConfigService } from '../../services/Config.service';
import {setSelectedCards} from "../../actions/playAction";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginTop: 20
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

export function Store(props) {
    const classes = useStyles();
    const userCards = useSelector((state) => state.storeReducer.userCards)
    const cardsToBuy = useSelector((state) => state.storeReducer.cardsToBuy)
    const dispatch = useDispatch()
    useEffect(() => {
        const fetchData = async () => {
            const apiUrl = await ConfigService.get(ConfigEnum.API_URL)

            const resp_cards = await fetch(
                `${apiUrl}/${props.url}`
            )
            const cardsToDisplay = await resp_cards.json()
            if (props.from === "Buy"){
                dispatch(setCardsToBuy(cardsToDisplay))
            }else if(props.from === "Sell" || props.from === "Play"){
                dispatch(setUserCards(cardsToDisplay))
            }
            dispatch(setSelectedCard(undefined))
            dispatch(setSelectedCards([]))
        }
        fetchData()
    }, [dispatch])

    if (userCards.length < 1 && cardsToBuy < 1) {
        return <div>pas de carte</div>
    }

    function SelectedCard(props) {
        const selectedCard = useSelector((state) => state.storeReducer.selectedCard)
        if (selectedCard === undefined) {
            return <div/>
        }
        return (
            <Card card={selectedCard} from={props.from}/>
        )
    }
    if (props.from === "Buy"){
        return (
            <div className={classes.root}>
                <Grid container spacing={3}>
                    <Grid item xs={24} sm={9}>
                        <CardList cards={cardsToBuy}/>
                    </Grid>
                    <Grid item xs={6} sm={3}>
                        <SelectedCard from={props.from}/>
                    </Grid>
                </Grid>
            </div>
        )
    }else if(props.from === "Sell" || props.from === "Play") {
        return (
            <div className={classes.root}>
                <Grid container spacing={3}>
                    <Grid item xs={24} sm={9}>
                        <CardList cards={userCards}/>
                    </Grid>
                    <Grid item xs={6} sm={3}>
                        <SelectedCard from={props.from}/>
                    </Grid>
                </Grid>
            </div>
        )
    }
}
