import React from 'react';
import {AppBar, Button, IconButton, makeStyles, Toolbar, Typography} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { navigationPage } from '../reducers/navigationReducer';
import { useDispatch, useSelector } from 'react-redux';
import { navigateAction } from '../actions/navigateAction';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { logoutAction } from '../actions/logoutAction';
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    textAlign: "center"
  },
  money: {
    marginRight: 20
  }
}));


export function NavBar() {
  const classes = useStyles();

  const dispatch = useDispatch()

  const onRegisterClick = () => {
    dispatch(navigateAction(navigationPage.REGISTER))
  } 
  
  const onLoginClick = () => {
    dispatch(navigateAction(navigationPage.LOGIN))
  }
  
  const onLogoutClick = () => {
    dispatch(logoutAction())
    dispatch(navigateAction(navigationPage.LOGIN))
  }

  const onSellClick = () => {
    dispatch(navigateAction(navigationPage.SELLCARDS))
  }

  const onBuyClick = () => {
    dispatch(navigateAction(navigationPage.BUYCARDS))
  }

  const onChatClick = () => {
    dispatch(navigateAction(navigationPage.CHAT))
  }

  const onPlayClick = () => {
    dispatch(navigateAction(navigationPage.PLAY))
  }

  const currentPage = useSelector((state) => state.navigationReducer.page)

  const loggedUser = useSelector((state) => state.loggedUserReducer.user)

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          {!loggedUser &&
          <React.Fragment>
            <Button color="inherit" onClick={onRegisterClick}>Register</Button>
            <Button color="inherit" onClick={onLoginClick}>Login</Button>
          </React.Fragment>
          }
          {loggedUser &&
          <React.Fragment>
            <Button color="inherit" onClick={onChatClick}>Chat</Button>
            <Button color="inherit" onClick={onSellClick}>Sell</Button>
            <Button color="inherit" onClick={onBuyClick}>Buy</Button>
            <Button color="inherit" onClick={onPlayClick}>Play</Button>
          </React.Fragment>
          }

          <Typography variant="h6" className={classes.title}>
            News {currentPage}
          </Typography>

          {loggedUser &&
          <React.Fragment>
            <div className={classes.money}>
              {loggedUser.account} $
            </div>
            <AccountCircleIcon/> {loggedUser.login}
            <Button color="inherit" onClick={onLogoutClick}>Logout</Button>
          </React.Fragment>
          }
        </Toolbar>
      </AppBar>
    </div>
  );
}