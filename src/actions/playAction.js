export const UPDATE_SELECTED_CARDS = 'UPDATE_SELECTED_CARDS'

export function setSelectedCards(playCards) {
    return { type: UPDATE_SELECTED_CARDS, playCards }
}