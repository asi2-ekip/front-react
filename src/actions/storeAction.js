export const UPDATE_SELECTED_CARD = 'UPDATE_SELECTED_CARD'
export const UPDATE_USER_CARDS = 'UPDATE_USER_CARDS'
export const UPDATE_CARDS_BUY = 'UPDATE_CARDS_BUY'

export function setUserCards(cards) {
    return { type: UPDATE_USER_CARDS, cards }
}

export function setSelectedCard(card) {
    return { type: UPDATE_SELECTED_CARD, card }
}

export function setCardsToBuy(cardsToBuy) {
    return { type: UPDATE_CARDS_BUY, cardsToBuy }
}