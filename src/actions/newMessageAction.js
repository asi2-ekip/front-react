import { chatAction } from "../reducers/chatReducer";


export function newMessageAction(infos){
    return {
        type: chatAction.NEW_MESSAGE,
        value: infos
    }
}


