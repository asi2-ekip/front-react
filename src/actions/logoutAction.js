import { loggedUserAction } from "../reducers/loggedUserReducer";
import { ChatService } from "../services/Chat.service";

export function logoutAction(){

    ChatService.logout();

    return {
        type: loggedUserAction.LOGOUT,
        value: null
    }
}
