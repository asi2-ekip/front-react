import { navigationAction } from "../reducers/navigationReducer";

export function navigateAction(page){
    return {
        type: navigationAction.NAVIGATE,
        value: page
    }
}