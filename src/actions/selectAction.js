import { chatAction } from "../reducers/chatReducer";


export function selectAction(infos){
    return {
        type: chatAction.SELECT,
        value: infos
    }
}


