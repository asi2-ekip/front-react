import { loggedUserAction } from "../reducers/loggedUserReducer";


export function loginAction(infos){
    return {
        type: loggedUserAction.LOGIN,
        value: infos
    }
}


