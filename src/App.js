import { Grid } from '@material-ui/core';
import {NavBar} from "./components/NavBar";
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { globalReducer } from './reducers'
import { MainPage } from './components/MainPage';
import { ConfigService } from './services/Config.service';
import { ConfigEnum } from './services/Config.service';

export function App(){
  
    const store = createStore(globalReducer,window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())

    ConfigService.get(ConfigEnum.API_URL).then(r => console.log(r))

    // store.subscribe(() => console.log("Page changed: ", store.getState().navigationReducer))
    // store.subscribe(() => console.log("SelectedCard changed: ", store.getState().storeReducer))
    // store.subscribe(() => console.log(store.getState().playReducer))
    // store.subscribe(() => console.log(store.getState().storeReducer))

    return (
      <Provider store={store}>
        <Grid container spacing={3}>
          <NavBar></NavBar>
        </Grid>

        <MainPage></MainPage>

      </Provider>
    )
}
