import { io, Socket } from "socket.io-client";
import { ConfigEnum, ConfigService } from "./Config.service";

export class ChatService {
    static socket = null;
    static lock = false;
    static isAuth = false;

    static async init(){

        // Already trying to connect
        if (ChatService.lock) {
            return;
        }

        // Already connected
        if (ChatService.socket){
            return;
        }

        ChatService.lock = true;
        const url = await ConfigService.get(ConfigEnum.CHAT_URL);

        ChatService.socket = io(url, {
            path: '/socket.io'
        })

        ChatService.lock = false;
    }

    static async auth(username){
        if(ChatService.isAuth){
            return;
        }

        ChatService.socket.emit('auth', username)
        ChatService.isAuth = true;

        // Reauth if chat service ask (ex: reboot)
        ChatService.socket.on('authRequest', ()=>{
            console.log('Auth requested');
            ChatService.socket.emit('auth', username)
        })
    }

    static async sendGlobal(message){
        ChatService.socket.emit('globalMessage', message)
    }

    static async sendPrivate(usernameDest, message){
        ChatService.socket.emit('privateMessage', usernameDest, message)
    }

    static async getUsers(){
        const url = await ConfigService.get(ConfigEnum.CHAT_URL);

        const result = await fetch(
            `${url}/api/chat/users`
        ).then(res => res.json())

        return result
    }

    static logout(){
        this.isAuth = false;
        this.socket.disconnect()
        delete this.socket
    }

}