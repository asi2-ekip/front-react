export class ConfigService {

    static configMap = null;
    static configFetchingPromise;

    static fetchConfig(){
        return fetch('/env/env.json')
        .then(res => res.json())
        .then(res => {
            ConfigService.configMap = new Map(Object.entries(res))
            console.info('Loaded config: ', ConfigService.configMap)
        })
    }

    static async get(envvar){

        if(!ConfigService.configMap){

            // Prevent multiple fetch and share the same promise
            if (!ConfigService.configFetchingPromise){
                ConfigService.configFetchingPromise = ConfigService.fetchConfig()
            }
            await ConfigService.configFetchingPromise
        }

        const result = ConfigService.configMap.get(envvar)
        if(!result){
            throw new Error('Unknown config: ', envvar)
        }

        return result
    }

}

// Enum
export const ConfigEnum = {
    API_URL: 'apiUrl',
    CHAT_URL: 'chatUrl',
    GAME_URL: 'gameUrl',
}