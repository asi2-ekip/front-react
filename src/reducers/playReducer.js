import { UPDATE_SELECTED_CARDS } from '../actions/playAction'

export const initialState = {
    selectedCards: []
}

export function playReducer(state = initialState, action) {
    switch (action.type) {
        case UPDATE_SELECTED_CARDS:
            return {...state, selectedCards: action.playCards};
        default:
            return state;
    }
}