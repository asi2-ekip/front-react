// ENUM
export const chatAction = {
    NEW_MESSAGE: 'newmessage',
    SELECT: 'select'
}

const initialState = {
    chats: {'global': []},
    selectedChat: 'global'
}

export function chatReducer(state = initialState, action) {
    switch (action.type) {
        case chatAction.NEW_MESSAGE:
            const chat = state.chats[action.value.dest] || []
            const newChat = [...chat, action.value]
            const newChats = {...state.chats}
            newChats[action.value.dest] = newChat
            return {...state, chats: newChats};
        case chatAction.SELECT:
            return {...state, selectedChat: action.value};
        default:
            return state;
    }
}