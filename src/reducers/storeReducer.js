import { UPDATE_SELECTED_CARD } from '../actions/storeAction'
import { UPDATE_USER_CARDS } from '../actions/storeAction'
import { UPDATE_CARDS_BUY } from '../actions/storeAction'

export const initialState = {
    selectedCard: undefined,
    userCards: [],
    cardsToBuy: []
}

export function storeReducer(state = initialState, action) {
    switch (action.type) {
        case UPDATE_SELECTED_CARD:
            return {...state, selectedCard: action.card};
        case UPDATE_USER_CARDS:
            return {...state, userCards: action.cards};
        case UPDATE_CARDS_BUY:
            return {...state, cardsToBuy: action.cardsToBuy};
        default:
            return state;
    }
}