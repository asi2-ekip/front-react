// ENUM
export const loggedUserAction = {
    LOGIN: 'login',
    LOGOUT: 'logout'
}

const initialState = {
    user: null,
    token: null
}

export function loggedUserReducer(state = initialState, action) {
    switch (action.type) {
        case loggedUserAction.LOGIN:
            return {...state, user: action.value.user, token: action.value.token};
        case loggedUserAction.LOGOUT:
            return {...state, user: null, token: null};
        default:
            return state;
    }
}