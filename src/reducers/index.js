import { combineReducers } from "redux";
import { chatReducer } from "./chatReducer";
import { loggedUserReducer } from "./loggedUserReducer";
import { navigationReducer } from "./navigationReducer";
import { storeReducer } from "./storeReducer";
import {playReducer} from "./playReducer";

export const globalReducer = combineReducers({
    navigationReducer: navigationReducer,
    loggedUserReducer: loggedUserReducer,
    storeReducer: storeReducer,
    playReducer: playReducer,
    chatReducer: chatReducer
})
