// ENUM
export const navigationPage = {
    LOGIN: 'login',
    REGISTER: 'register',
    SELLCARDS: 'sellcards',
    BUYCARDS: 'buycards',
    CHAT: 'chat',
    PLAY: 'play',
    GAME: 'game'
}

export const navigationAction = {
    NAVIGATE: 'navigate'
}

const initialState = {
    page: navigationPage.LOGIN
}

export function navigationReducer(state = initialState, action) {
    switch (action.type) {
        case navigationAction.NAVIGATE:
            return {...state, page: action.value};
        default:
            return state;
    }
}