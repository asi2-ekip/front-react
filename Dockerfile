FROM nginx:1.19.3-alpine

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x entrypoint.sh

WORKDIR /usr/share/nginx/html

COPY build .

EXPOSE 80

ENTRYPOINT [ "/entrypoint.sh" ]