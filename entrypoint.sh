#!/bin/sh
ENV_PATH="/usr/share/nginx/html/env"

# Apply default env var
set -a
source $ENV_PATH/default.conf
set +a
# Apply env vars to env.json template
envsubst < $ENV_PATH/env.template.json > $ENV_PATH/env.json
# Log config
echo RUNNING WITH CONFIG:
cat $ENV_PATH/env.json

# Run default nginx entrypoint
exec /docker-entrypoint.sh nginx -g "daemon off;"
